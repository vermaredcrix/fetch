import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { VendorEditOrderPageRoutingModule } from './vendor-edit-order-routing.module';

import { VendorEditOrderPage } from './vendor-edit-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    VendorEditOrderPageRoutingModule
  ],
  declarations: [VendorEditOrderPage]
})
export class VendorEditOrderPageModule {}
