import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorEditOrderPage } from './vendor-edit-order.page';

const routes: Routes = [
  {
    path: '',
    component: VendorEditOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorEditOrderPageRoutingModule {}
