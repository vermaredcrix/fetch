import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendorEditOrderPage } from './vendor-edit-order.page';

describe('VendorEditOrderPage', () => {
  let component: VendorEditOrderPage;
  let fixture: ComponentFixture<VendorEditOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorEditOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendorEditOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
