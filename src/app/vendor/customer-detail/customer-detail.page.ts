import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NavController, Platform } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.page.html',
  styleUrls: ['./customer-detail.page.scss'],
})
export class CustomerDetailPage implements OnInit {
  store_details : any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
    public navCtrl: NavController,
  ) {
    this.load_storage_data();
   }

  ngOnInit() {
  }



  async load_storage_data(){

    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });




      await this.api.WCV2getAgentDeliveries('customerdetails?vendor_id=' + this.vendorID).subscribe(res => {

          console.log('======================'+res);
     
          let ob = JSON.parse(JSON.stringify(res));
          this.store_details =  Object.values(ob['response']);


          console.log(this.store_details);


      }, err => {

          console.log(err);

      });

  }


  getDetail(order) {

    console.log(order);
    // console.log(order.order_detail.order_id);

    let orderX =


    {
        id : order.order_id,
        status : status,
        customer_note: "",
        date_created: order._paid_date,
        total: order._order_total,
        discount_total: order._cart_discount,

        shipping_tax:order._order_shipping_tax,
        shipping_total: order._order_shipping,
        total_tax: order._order_tax,
        
        billing:
        {




            address_1: order._billing_address_1     ,
            address_2:order._billing_address_2     ,
            city: order._billing_city     ,
            company: order._billing_company     ,
            country:order._billing_country     ,
            email: order._billing_email     ,
            first_name: order._billing_first_name     ,
            last_name: order._billing_last_name     ,
            phone: order._billing_phone     ,
            postcode: order._billing_postcode     ,
            state: order._billing_state     ,
        },
    
        shipping:
        {
            address_1:  order._shipping_address_1     ,
            address_2: order._shipping_address_2     ,
            city:order._shipping_city     ,
            company: order._shipping_company     ,
            country: order._shipping_country    ,
            first_name: order._shipping_first_name     ,
            last_name: order._shipping_last_name     ,
            postcode:order._shipping_postcode     ,
            state: order._shipping_state     ,
        },

        line_items : [{
            name: order.product_details.product_name,
            price: order.product_details.product_price,
            quantity:order.product_details._qty,
            total: order.product_details.item_total,
        }]
    };

    localStorage.removeItem('orderData');
    this.storage.remove('orderData'); 
    localStorage.setItem('orderData', JSON.stringify(order));
    this.storage.set('orderData', order); 


    let navigationExtras: NavigationExtras = {
        queryParams: {
            order: orderX
        }
    };
    this.navCtrl.navigateForward('/vendor-edit-order/' + order.order_id);
}


}
