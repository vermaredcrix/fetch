import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-vendor-coupon-add',
  templateUrl: './vendor-coupon-add.page.html',
  styleUrls: ['./vendor-coupon-add.page.scss'],
})
export class VendorCouponAddPage implements OnInit {

  vendorID : any;
  isVendorLogin:any;
  coupon_Add : any = [];
  errorTxt  = '';
  constructor(
    public api: ApiService, 
    public modalController: ModalController,
    private storage: Storage,
  ) {

    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });

   }

  ngOnInit() {
  }

  async Add_coupon (){


  

  // console.log(vendor_product_add);
  
  await this.api.WCV2getAgentDeliveries('createcoupons?vendor_id='
  
  +this.vendorID+ 
  '&coupon_code=' + this.coupon_Add.coupon_code +
  '&amount=' + this.coupon_Add.amount +
  '&discount_type=' + this.coupon_Add.discount_type +
  '&expiry_date=' + this.coupon_Add.expiry_date 

 ).subscribe(res => {
      console.log('====', res);
      this.coupon_Add = [];

      if(res['status'] = 'error'){
        console.log('error');
        this.errorTxt= res['msg'];
       }

       if(res['status'] = 'ok'){
        this.errorTxt = '';
        this.errorTxt= res['msg'];
   

       }




  }, err => {
      console.log(err);
  });
  }


  dismissM() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }
  



}
