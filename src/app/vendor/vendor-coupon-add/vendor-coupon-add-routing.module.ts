import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorCouponAddPage } from './vendor-coupon-add.page';

const routes: Routes = [
  {
    path: '',
    component: VendorCouponAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorCouponAddPageRoutingModule {}
