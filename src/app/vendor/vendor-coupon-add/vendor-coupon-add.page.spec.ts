import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VendorCouponAddPage } from './vendor-coupon-add.page';

describe('VendorCouponAddPage', () => {
  let component: VendorCouponAddPage;
  let fixture: ComponentFixture<VendorCouponAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorCouponAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VendorCouponAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
