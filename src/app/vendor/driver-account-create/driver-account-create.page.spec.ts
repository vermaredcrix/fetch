import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DriverAccountCreatePage } from './driver-account-create.page';

describe('DriverAccountCreatePage', () => {
  let component: DriverAccountCreatePage;
  let fixture: ComponentFixture<DriverAccountCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverAccountCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DriverAccountCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
