import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-order-list',
    templateUrl: './order-list.page.html',
    styleUrls: ['./order-list.page.scss'],
})
export class OrderListPage implements OnInit {
    filter: any = {};
    orders: any = [];
    hasMoreItems: boolean = true;
    loader: boolean = false;
    vendorID: any;
    isVendorLogin: any;

    constructor(public platform: Platform,
        public api: ApiService,
        private storage: Storage,
        public settings: Settings, public router: Router, public navCtrl: NavController, public route: ActivatedRoute) {
        this.filter.page = 1;
        this.filter.vendorid = this.settings.customer.id;

        this.vendorID = localStorage.getItem('vendorID');
        this.isVendorLogin = localStorage.getItem('isVendorLogin');

        this.storage.get('isVendorLogin').then((val) => {
            this.isVendorLogin = val;
        });

        this.storage.get('vendorID').then((val) => {
            this.vendorID = val;
        });

    }
    ngOnInit() {
        //THIS WORKS FOE WCFM ALSO, DO NOT CHANEG THIS. WCFM API NOT WORKING
        this.getOrders__();

        //WCFM DO NOT USE THIS. WCFM API THIS IS NOT WORKING
        //this.getWCFMOrders();
    }


    async getOrders__() {
        this.loader = true;
        await this.api.WCV2getAgentDeliveries('getvendororders?vendor_id=' + this.vendorID).subscribe(res => {
            this.loader = false;

            this.orders = res["response"];
            console.log("Vedev Orders:",this.orders);


        }, err => {
            this.loader = false;
            console.log(err);

        });
    }



    // getOrders() {
    //     console.log(this.filter);

    //     this.loader = true;
    //     if (this.platform.is('hybrid'))
    //     this.api.getItemIonic('orders', this.filter).then((res) => {
    //         this.orders = res;
    //         this.loader = false;
    //     }, err => {
    //         console.log(err);
    //     });
    //     else {
    //        this.api.getItem('orders', this.filter).subscribe(res => {
    //             this.orders = res;
    //             this.loader = false;
    //         }, err => {
    //             console.log(err);
    //         }); 
    //     }
    // }
    loadData(event) {
        this.filter.page = this.filter.page + 1;
        if (this.platform.is('hybrid'))
            this.api.getItemIonic('orders', this.filter).then((res) => {
                this.orders.push.apply(this.orders, res);
                event.target.complete();
                if (!res) this.hasMoreItems = false;
            }, err => {
                event.target.complete();
            });
        else {
            this.api.getItem('orders', this.filter).subscribe(res => {
                this.orders.push.apply(this.orders, res);
                event.target.complete();
                if (!res) this.hasMoreItems = false;
            }, err => {
                event.target.complete();
            });
        }
    }

    getDetail(order, status) {

        console.log(order);
        console.log(order.order_detail.order_id);

        let orderX =


        {
            id : order.order_detail.order_id,
            status : status,
            customer_note: "",
            date_created: order.order_detail._paid_date,
            total: order.order_detail._order_total,
            discount_total: order.order_detail._cart_discount,

            shipping_tax:order.order_detail._order_shipping_tax,
            shipping_total: order.order_detail._order_shipping,
            total_tax: order.order_detail._order_tax,
            
            billing:
            {




                address_1: order.order_detail._billing_address_1     ,
                address_2:order.order_detail._billing_address_2     ,
                city: order.order_detail._billing_city     ,
                company: order.order_detail._billing_company     ,
                country:order.order_detail._billing_country     ,
                email: order.order_detail._billing_email     ,
                first_name: order.order_detail._billing_first_name     ,
                last_name: order.order_detail._billing_last_name     ,
                phone: order.order_detail._billing_phone     ,
                postcode: order.order_detail._billing_postcode     ,
                state: order.order_detail._billing_state     ,
            },
        
            shipping:
            {
                address_1:  order.order_detail._shipping_address_1     ,
                address_2: order.order_detail._shipping_address_2     ,
                city:order.order_detail._shipping_city     ,
                company: order.order_detail._shipping_company     ,
                country: order.order_detail._shipping_country    ,
                first_name: order.order_detail._shipping_first_name     ,
                last_name: order.order_detail._shipping_last_name     ,
                postcode:order.order_detail._shipping_postcode     ,
                state: order.order_detail._shipping_state     ,
            },

            line_items : [{
                name: order.order_detail.product_details.product_name,
                price: order.order_detail.product_details.product_price,
                quantity:order.order_detail.product_details._qty,
                total: order.order_detail.product_details.item_total,
            }]
        };

        localStorage.removeItem('orderData');
        this.storage.remove('orderData'); 
        localStorage.setItem('orderData', JSON.stringify(order));
        this.storage.set('orderData', order); 


        let navigationExtras: NavigationExtras = {
            queryParams: {
                order: orderX
            }
        };
        this.navCtrl.navigateForward('/vendor-edit-order/' + order.order_detail.order_id);
    }
    editOrder(order) {
        this.navCtrl.navigateForward('/tabs/account/vendor-orders/edit-order/' + order.id);
    }

    //WCFM
    /*getWCFMOrders(){
        this.loader = true;
        if (this.platform.is('hybrid'))
        this.api.getWCFMIonic('orders', this.filter).then((res) => {
            this.orders = res;
            this.loader = false;
        }, err => {
            console.log(err);
        });
        else {
            this.api.getWCFM('orders', this.filter).subscribe(res => {
                this.orders = res;
                this.loader = false;
            }, err => {
                console.log(err);
            });
        }
    }
    loadData(event) {
        this.filter.page = this.filter.page + 1;
        if (this.platform.is('hybrid'))
            this.api.getWCFMIonic('orders', this.filter).then((res) => {
                this.orders.push.apply(this.orders, res);
                event.target.complete();
                if (!res) this.hasMoreItems = false;
            }, err => {
                event.target.complete();
            });
        else {
            this.api.getWCFM('orders', this.filter).subscribe(res => {
                this.orders.push.apply(this.orders, res);
                event.target.complete();
                if (!res) this.hasMoreItems = false;
            }, err => {
                event.target.complete();
            });
        }
    }*/


    async doRefresh(event) {
        await this.api.WCV2getAgentDeliveries('getvendororders?vendor_id=' + this.vendorID).subscribe(res => {
    
    
            this.orders = res["response"];
            console.log(this.orders);
    
            setTimeout(() => {
                event.target.complete();
            }, 2000);
            
        }, err => {
    
            console.log(err);
    
        });

   
    }

    filterItems(searchTerm) {
        return this.orders.filter(item => {
         return item.order_detail.product_details[0].product_name.indexOf(searchTerm
            ) > -1;
          return item.order_detail.product_details[0].product_name.indexOf(searchTerm) > -1;
        });
      }



    

}