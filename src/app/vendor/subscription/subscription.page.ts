import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage implements OnInit {

  membership = false;
  apiData:any;
  text: any;
  sData:any;
  jsonData:any;
  postData = {
    user_id: 90
  }
  subs=[]=new Array();
  url = "https://fetch.betaplanets.com/wp-json/mobileapi/v1/getsubscription";

  constructor(public https: HttpClient) {
    console.log(this.postData);
    this.data();
  }

  ngOnInit() {}
  async data() {

    await this.https.post(this.url, this.postData).subscribe(data => {
      if (data['status'] == 'error') {
       
      } else {
        //console.log(data['response']['all_membership']);
        this.apiData=data['response'];
        console.log("Data JSON::",data);
        this.text = data['status'];
        //this.jsonData=JSON.stringify(this.apiData);
        console.log("API Data 1 Time :::",this.apiData["all_membership"][0]);
        this.jsonData=JSON.stringify(data['response']);
        this.sData=JSON.parse(this.jsonData);
        console.log("New API Data:",this.sData['all_membership']);
        // let sData=JSON.stringify(this.apiData['all_membership']);
        // this.jsonData=JSON.parse(sData);
        // console.log("Sample ID:::",this.jsonData);
        // console.log("Size of JSON Data is:",this.jsonData);
        // for(var i=0;i<3;i++){
        //  this.subs.push({mem_id:this.jsonData[i]['membership_id']});
        // }
        // console.log("Subscribers:",this.subs);
      }
    });
  }

}