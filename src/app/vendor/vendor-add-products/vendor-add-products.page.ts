import { Component, OnInit } from '@angular/core';
import { PhotosPage } from '../product-add/photos/photos.page';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../api.service';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Config } from '../../../app/config';


@Component({
  selector: 'app-vendor-add-products',
  templateUrl: './vendor-add-products.page.html',
  styleUrls: ['./vendor-add-products.page.scss'],
})
export class VendorAddProductsPage implements OnInit {
  uploadingImageSpinner: boolean = false;
  photos: any;
  imageresult: any;
  imageIndex: any = 0;
  items: any = {};
	subcategories: any = [];
  categories: any = [];
  vendor : any = [];

  vendorID : any;
  isVendorLogin:any;
catListing : any;

  constructor(    
    public api: ApiService, 
    public modalController: ModalController,
    private storage: Storage,
    private crop: Crop,
    private imagePicker: ImagePicker,
    private transfer: FileTransfer,
    public config: Config,
    ) {


      this.vendorID = localStorage.getItem('vendorID');
      this.isVendorLogin = localStorage.getItem('isVendorLogin');

      this.storage.get('isVendorLogin').then((val) => {
          this.isVendorLogin = val;
      });

      this.storage.get('vendorID').then((val) => {
          this.vendorID = val;
      });

      this.getCategories();

 
   }

  ngOnInit() {
  }


  async next(){

    console.log(this.vendor);
    

  const modal = await this.modalController.create({
    component: PhotosPage,
    // componentProps: {
    //     pass: arr
    // }
});
return await modal.present();
  }

  async addProducts_() {

    console.log(this.vendor);


// product_description: "Long" 
// product_manage_stock: true

// category
// product_short_description: "Short"

// product_type: "external"
// product_weight: "20"


    let vendor_product_add = 
      {
        vendor_id : this.vendorID,
        title : this.vendor.product_type,
        Product : this.vendor.product_name,
        description : this.vendor.product_description,
        price :this.vendor.product_regular_price,
        sale_price : this.vendor.product_sale_price,
        sku : this.vendor.sku,
        stock : this.vendor.product_stock_quantity,
        category : this.vendor.category,
        // thumbnail_id : '125258',
        // product_gallery : '125265,125263',
      }
    

    console.log(vendor_product_add);
    
    await this.api.addProducts_('addproduct', vendor_product_add).subscribe(res => {
        console.log('====', res);
   

    }, err => {
        console.log(err);
    });
}
// // =100&=&=10&=&=&=

//   https://fetch.betaplanets.com/wp-json/mobileapi/v1/addproduct?vendor_id=7&title=Test%20Product&description=loreum%20ipsum%20dummy%20content&price=150&sale_

  // getCategory(ID, slug, name){
  //   this.subcategories = [];
  //     this.vendor.product.categories[0] = {id: ID};     
  //     this.items.id = ID;
  //     this.items.name = name;
  //     this.items.slug = slug;
  //     this.items.categories = this.data.categories;

  //     for(let item in this.items.categories){
  //         if(this.items.categories[item].parent == ID){
  //             this.subcategories.push(this.items.categories[item]);
  //         }
  //     }

  //     if(this.subcategories.length){
  //        this.navCtrl.navigateForward('/tabs/account/add-products/subcategory/' + ID);
  //     }

  //     else this.navCtrl.navigateForward('/tabs/account/add-products/details/' + ID);
  // }



  // async presentAlert(message) {
  //   const alert = await this.alertController.create({
  //     header: 'Alert',
  //     message: message,
  //     buttons: ['OK']
  //   });

  //   await alert.present();
  // }


  async getCategories(){

    await this.api.WCV2getAgentDeliveries('getcategories').subscribe(res => {
      console.log('====', res);
      this.catListing = res["response"];

  }, err => {
      console.log(err);
  });
  }


  picker() {
    let options = {
        maximumImagesCount: 1,
    }
    this.photos = new Array < string > ();
    this.imagePicker.getPictures(options).then((results) => {
        this.reduceImages(results).then((results) => this.handleUpload(results));
    }, (err) => {
        console.log(err)
    });
}

handleUpload(results) {
  this.upload();
}
reduceImages(selected_pictures: any): any {
  return selected_pictures.reduce((promise: any, item: any) => {
      return promise.then((result) => {
          return this.crop.crop(item, {
              quality: 75,
              targetHeight: 100,
              targetWidth: 100
          }).then(cropped_image => this.photos = cropped_image);
      });
  }, Promise.resolve());
}


upload() {
  this.uploadingImageSpinner = true;
  const fileTransfer: FileTransferObject = this.transfer.create();
  var headers = new Headers();
  headers.append('Content-Type', 'multipart/form-data');
  let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: 'name.jpg',
      headers: {
          headers
      }
  }


  fileTransfer.upload(this.photos, this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp_upload_image', options).then((data) => {
      this.uploadingImageSpinner = false;
      this.imageresult = JSON.parse(data.response);
      this.vendor.product.images[this.imageIndex] = {};
      this.vendor.product.images[this.imageIndex].src = this.imageresult.url;
      this.imageIndex = this.imageIndex + 1;

      alert('fileTransfer imageresult'+this.imageresult);
      // success
  }, (err) => {
      alert( 'fileTransfer ERROR'+err);
  })


  alert('what already sent in options===>'+options);
  alert('To send Photos along URL and options=>'+this.photos);
}




}
