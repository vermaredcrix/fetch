import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { VendorAddProductsPageRoutingModule } from './vendor-add-products-routing.module';

import { VendorAddProductsPage } from './vendor-add-products.page';
import { PhotosPage } from './../product-add/photos/photos.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    VendorAddProductsPageRoutingModule,

  ],
  declarations: [VendorAddProductsPage,
    PhotosPage],


    entryComponents :[
      PhotosPage],
})
export class VendorAddProductsPageModule {}
