import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { Settings } from './../../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-store',
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.scss'],
})
export class StorePage implements OnInit {
  store_details_:any;
  store_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) { 


    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    this.store_details_= JSON.parse(localStorage.getItem('storeData'));

    this.storage.get('storeData').then((val) => {
        this.store_details_ = val;
    });

    console.log(this.store_details_);
    

    let ax = [];
    ax = this.store_details_;

    this.store_details__ = [];
    this.store_details__.push(ax);

      console.log(this.store_details__[0]);

      this.store_details = this.store_details__[0];

//       store_hide_address: "yes"
// store_hide_description: "yes"
// store_hide_email: "yes"
// store_hide_map: "yes"
// store_hide_phone: "yes"
// store_hide_policy: "yes"
      
  }

  ngOnInit() {

  }

  async updateVendorDetail() {
    this.Mess = '';
    const loading = await this.loadingController.create({
        message: 'Loading...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
    });
    await loading.present();


    let store_update = 
      {
        vendor_id : this.vendorID,
        store_name : this.store_details.store_name,
        store_slug : this.store_details.store_slug,
        store_email : this.store_details.store_email,
        store_phone : this.store_details.phone,

        banner_type : this.store_details.banner_type,
        list_banner_type :this.store_details.list_banner_type,

        store_hide_address : this.store_details.store_hide_address,
        store_hide_description : this.store_details.store_hide_description,
        store_hide_email : this.store_details.store_hide_email,
        store_hide_phone : this.store_details.store_hide_phone,
        store_hide_map : this.store_details.store_hide_map,
        store_hide_policy : this.store_details.store_hide_policy,
        // thumbnail_id : '125258',
        // product_gallery : '125265,125263',
      }
    console.log(store_update);
    
    await this.api.update_POST_parms('updatevendorstore', store_update).subscribe(res => {
        console.log('====', res);
        this.Mess = 'Info updated.';
        this.loadData();
        loading.dismiss();

    }, err => {
      this.Mess = 'Info not updated.';
      loading.dismiss();
        console.log(err);
    });


}


async loadData() {
  var loc = await this.getCurrentCusData();

}

getCurrentCusData() {
  return new Promise(resolve => {

      return this.api.WCV2getItem('customers/' + this.vendorID).subscribe(res => {
          this.settings.customer = res;
          resolve(res)
          console.log(this.settings.customer);

          if (this.isVendorLogin = true) {

          let ax = [];
          ax.push(this.settings.customer);
          var filter = "key";
          var keyword = "wcfmmp_profile_settings";
      
          var filteredData = ax[0].meta_data.filter(function(obj) {
            return obj[filter] === keyword;
          });
            let makObj = [];
            makObj = filteredData[0].value;
          console.log(makObj);


          localStorage.removeItem('storeData');
          this.storage.remove('storeData'); 
          localStorage.setItem('storeData', JSON.stringify(makObj));
          this.storage.set('storeData', makObj); 

          console.log('res=========',makObj['store_name']);
          

      }
          return res;

      }, err => {
          console.log(err);
      });

  });
}


}
