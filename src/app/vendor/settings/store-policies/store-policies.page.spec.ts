import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StorePoliciesPage } from './store-policies.page';

describe('StorePoliciesPage', () => {
  let component: StorePoliciesPage;
  let fixture: ComponentFixture<StorePoliciesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePoliciesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StorePoliciesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
