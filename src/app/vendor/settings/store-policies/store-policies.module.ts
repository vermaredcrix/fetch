import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { StorePoliciesPageRoutingModule } from './store-policies-routing.module';

import { StorePoliciesPage } from './store-policies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StorePoliciesPageRoutingModule,
    TranslateModule
  ],
  declarations: [StorePoliciesPage]
})
export class StorePoliciesPageModule {}
