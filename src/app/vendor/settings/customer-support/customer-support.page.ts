import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';
import { Settings } from './../../../data/settings';
import { LoadingController} from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-customer-support',
  templateUrl: './customer-support.page.html',
  styleUrls: ['./customer-support.page.scss'],
})
export class CustomerSupportPage implements OnInit {
  store_details_:any;
  store_details:any;
  store_details__:any;
  vendorID: any;
  isVendorLogin: any;
  Mess = '';
  paymentMethods = [];
  address: any = [];
  countries: any;
  states: any;
  billingStates: any;
  shippingStates: any;

  constructor(
    public settings: Settings,
    public loadingController: LoadingController, 
    public api: ApiService,
    private storage: Storage,
  ) { 
    this.load_storage_data();
      this.getCountries();
  }

  ngOnInit() {
  }

  load_storage_data(){
    this.vendorID = localStorage.getItem('vendorID');
    this.isVendorLogin = localStorage.getItem('isVendorLogin');

    this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;
    });

    this.storage.get('vendorID').then((val) => {
        this.vendorID = val;
    });


    this.store_details_= JSON.parse(localStorage.getItem('storeData'));

    this.storage.get('storeData').then((val) => {
        this.store_details_ = val;
    });

    let ax = [];
    ax = this.store_details_;

    this.store_details__ = [];
    this.store_details__.push(ax);

      console.log(this.store_details__[0]);

      this.store_details = this.store_details__[0];

      this.paymentMethods = [
        {value: "bank_transfer" , label : 'Bank Transfer'},
        {value: "paypal" , label : 'PayPal'},

      ]
  }



  async updateVendorDetail() {
    this.Mess = '';
    const loading = await this.loadingController.create({
        message: 'Loading...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
    });
    await loading.present();


    let store_update = 
      {
        vendor_id : this.vendorID,
        customer_support_phone : this.store_details.customer_support.phone,
        customer_support_email : this.store_details.customer_support.email,
        customer_support_address1 : this.store_details.customer_support.address1,

        customer_support_address2: this.store_details.customer_support.address2,
        customer_support_country: this.store_details.customer_support.country,
        customer_support_city: this.store_details.customer_support.city,
        customer_support_state: this.store_details.customer_support.state,
        customer_support_zip: this.store_details.customer_support.zip,

      }
    console.log(store_update);
    
    await this.api.update_POST_parms('updatevendorstore', store_update).subscribe(res => {
        console.log('====', res);
        this.Mess = 'Info updated.';
        this.loadData();
        loading.dismiss();

    }, err => {
      this.Mess = 'Info not updated.';
      loading.dismiss();
        console.log(err);
    });


}


async loadData() {
  var loc = await this.getCurrentCusData();

}

getCurrentCusData() {
  return new Promise(resolve => {

      return this.api.WCV2getItem('customers/' + this.vendorID).subscribe(res => {
          this.settings.customer = res;
          resolve(res)
          console.log(this.settings.customer);

          if (this.isVendorLogin = true) {

          let ax = [];
          ax.push(this.settings.customer);
          var filter = "key";
          var keyword = "wcfmmp_profile_settings";
      
          var filteredData = ax[0].meta_data.filter(function(obj) {
            return obj[filter] === keyword;
          });
            let makObj = [];
            makObj = filteredData[0].value;
          console.log(makObj);


          localStorage.removeItem('storeData');
          this.storage.remove('storeData'); 
          localStorage.setItem('storeData', JSON.stringify(makObj));
          this.storage.set('storeData', makObj); 

          console.log('res=========',makObj['store_name']);
          

      }
          return res;

      }, err => {
          console.log(err);
      });

  });
}

async getCountries() {
  await this.api.postItem('countries').subscribe(res => {
    

      this.countries = res;
      if (this.countries && this.countries.length == 1) {
        console.log(res);
          this.address['billing_country'] = this.countries[0].value;
          this.address['shipping_country'] = this.countries[0].value;
          this.billingStates = this.countries.find(item => item.value == this.store_details.customer_support.country);
          // this.shippingStates = this.countries.find(item => item.value == this.address['billing_country']);
      } else {
          this.billingStates = this.countries.find(item => item.value == this.store_details.customer_support.country);
          // this.shippingStates = this.countries.find(item => item.value == this.settings.customer.shipping.country);
      }
  }, err => {
      console.log(err);
  });


}

getBillingRegion() {
  this.billingStates = this.countries.find(item => item.value == this.store_details.customer_support.country);
  
  this.store_details.customer_support.state = '';

}



}
