import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { SeoPageRoutingModule } from './seo-routing.module';

import { SeoPage } from './seo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeoPageRoutingModule,
    TranslateModule
  ],
  declarations: [SeoPage]
})
export class SeoPageModule {}
