import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StoreHoursPage } from './store-hours.page';

describe('StoreHoursPage', () => {
  let component: StoreHoursPage;
  let fixture: ComponentFixture<StoreHoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreHoursPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StoreHoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
