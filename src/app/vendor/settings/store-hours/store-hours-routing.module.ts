import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoreHoursPage } from './store-hours.page';

const routes: Routes = [
  {
    path: '',
    component: StoreHoursPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoreHoursPageRoutingModule {}
