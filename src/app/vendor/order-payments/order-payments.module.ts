import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { OrderPaymentsPageRoutingModule } from './order-payments-routing.module';

import { OrderPaymentsPage } from './order-payments.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPaymentsPageRoutingModule,
    TranslateModule
  ],
  declarations: [OrderPaymentsPage]
})
export class OrderPaymentsPageModule {}
