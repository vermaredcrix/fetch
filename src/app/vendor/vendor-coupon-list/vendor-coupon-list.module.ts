import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { VendorCouponListPageRoutingModule } from './vendor-coupon-list-routing.module';

import { VendorCouponListPage } from './vendor-coupon-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VendorCouponListPageRoutingModule,
    TranslateModule
  ],
  declarations: [VendorCouponListPage]
})
export class VendorCouponListPageModule {}
