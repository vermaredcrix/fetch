import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { Vendor } from './../../data/vendor';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.page.html',
  styleUrls: ['./vendor-list.page.scss'],
})
export class VendorListPage implements OnInit {
    vendors: any = [];
    filter: any = {};
    hasMoreItems: boolean = true;
    tempVendor: any = [];
    newArray:any;
    usersLocation = {};
    loggedIn :any;

    constructor(public platform: Platform, 
        private storage: Storage,
        public api: ApiService, public settings: Settings, public router: Router, public navCtrl: NavController, public route: ActivatedRoute, public vendor: Vendor) {
      this.filter.page = 1;
      this.filter.per_page = 30;
      this.filter.wc_vendor = true;

      this.loggedIn = localStorage.getItem('logginInVal');
      this.storage.get('logginInVal').then((val) => {
        this.loggedIn = val;
    });

    }

    ngOnInit() {
        //WCFM
        this.getWCFMVendors();
    }
    
    /* WC Marketplace */
    async getWcVendors() {
        if (this.platform.is('hybrid'))
        await this.api.WCMPVendorIonic('vendors', this.filter).then((res) => {
            this.vendors = res;
        }, err => {
            console.log(err);
        });
        else
        await this.api.WCMPVendor('vendors', this.filter).subscribe(res => {
            this.vendors = res;
        }, err => {
            console.log(err);
        });
    }
    async loadData(event) {
        this.filter.page = this.filter.page + 1;
        
        /* WCFM */
        await this.api.postItem('wcfm-vendor-list', this.filter).subscribe(res => {
            this.tempVendor = res;
            this.vendors.push.apply(this.vendors, res);
            event.target.complete();
            if (this.tempVendor && this.tempVendor.length == 0) this.hasMoreItems = false;
            else if (!this.tempVendor || !this.tempVendor.length) event.target.complete();

          
        }, err => {
            event.target.complete();
        });


    }
    async getDokanVendors() {
        await this.api.postItem('vendors-list', this.filter).subscribe(res => {
            this.vendors = res;
        }, err => {
            console.log(err);
        });
    }
    async getWCFMVendors() {
        await this.api.postItem('wcfm-vendor-list', this.filter).subscribe(res => {
            // console.log('VENDOR LIS'+JSON.stringify(res));
            
            // this.vendors = res;
            this.commonFiltere();
        }, err => {
            console.log(err);
        });
    }
    getDetail(item) {
        console.log('========>>', item);
        
        this.vendor.vendor = item;
        this.navCtrl.navigateForward('/tabs/vendor/products');
    }



    // =======

    commonFiltere(){

        console.log('ENTER--');
        

        const newArray = this.vendors.map(o => {
            return [o.store_info.store_lat, o.store_info.store_lng];
        });


        this.newArray = newArray;

        return new Promise(resolve => {
            this.vendors = this.applyHaversine(this.vendors);


            console.log('UPDATE?', this.vendors);

          
            this.vendors.sort((locationA, locationB) => {
                return locationA.distance - locationB.distance;
            });

            resolve(this.vendors);

            let Fil = this.vendors.filter(
                f => parseFloat(f.distance) <= parseInt(f.store_info.address.delivery_radius));
            this.vendors = '';
            this.vendors = Fil;
            
            console.log('Filtered??', this.vendors);
        });
    }




    applyHaversine(locations) {

        // this.usersLocation = {};

        locations.map((location) => {

            let placeLocation = {
                id: location.id,
                lat: location.store_info.store_lat,
                lng: location.store_info.store_lng,
                delivery_radius: location.store_info.address.delivery_radius
            };

            // console.log(this.usersLocation); ======
            // console.log('????',placeLocation);
            
            if (this.loggedIn == 'true') {

     
                this.usersLocation = {};
                if(this.settings.customer.cus_lat){
                    this.usersLocation = {};
                    this.usersLocation = {
                        lat : this.settings.customer.cus_lat,
                        lng : this.settings.customer.cus_lng
                    }
        
                    console.log(this.usersLocation);
                    
               
                }
            } 
        
            else{
                this.navCtrl.navigateForward('/tabs/account/register');
            }

            console.log(this.usersLocation);
            
        
            location.distance = this.getDistanceBetweenPoints(
                this.usersLocation,
                placeLocation,
            ).toFixed(2);
        });
        return locations;
    }

    getDistanceBetweenPoints(start, end) {

        let earthRadius = {
            miles: 3958.8,
            km: 6371
        };

        let R = 3958.8;
        let lat1 = start.lat;
        let lon1 = start.lng;
        let lat2 = end.lat;
        let lon2 = end.lng;

        let dLat = this.toRad((lat2 - lat1));
        let dLon = this.toRad((lon2 - lon1));
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
            Math.sin(dLon / 2) *
            Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;

        return d;

    }

    toRad(x) {
        return x * Math.PI / 180;
    }

}
