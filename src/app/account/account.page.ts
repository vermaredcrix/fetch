import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Settings } from './../data/settings';
import { ApiService } from './../api.service';
import { AppRate } from '@ionic-native/app-rate/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Config } from './../config';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-account',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})
export class AccountPage {
    toggle: any;
    isDeliveryAgent:any;
    loggedIn:any;
    user_id:any;
    isVendorLogin:any;
    UserLoggedIn = true;
    constructor(private statusBar: StatusBar, 
        private storage: Storage, private config: Config, public api: ApiService, public navCtrl: NavController, public settings: Settings, public platform: Platform, private appRate: AppRate, private emailComposer: EmailComposer, private socialSharing: SocialSharing) {
           
           
            // this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent');
            // this.storage.get('isDeliveryAgent').then((val) => {
            //     // console.log('isDeliveryAgent', val);
            //     this.isDeliveryAgent = val;
            //   });

            
            //  console.log(this.isDeliveryAgent);

            this.UserLoggedIn = true;

        this.storage.get('logginInVal').then((val) => {
            this.loggedIn = val;
        });
        this.loggedIn = localStorage.getItem('logginInVal');
        this.storage.get('user_id').then((val) => {
            this.user_id = val;
        });
        this.user_id = localStorage.getItem('user_id');

        // at login time. 
     
        if (this.isDeliveryAgent == 'true') {
            this.settings.customer.id = this.user_id;

        }
        else {

            console.log('== > new user');
         
         
        }



        this.isVendorLogin = localStorage.getItem('isVendorLogin');

        this.storage.get('isVendorLogin').then((val) => {
            this.isVendorLogin = val;
        });
  
  
        if (this.isVendorLogin == 'true') {
          this.UserLoggedIn = false;
  
  
        }



          


              
        }



    goTo(path) {
        // localStorage.clear();
        if(path === 'tabs/account/address/edit-address'){
           console.log('setState');
           localStorage.setItem('accountEdit','true');
            
        }
        this.navCtrl.navigateForward(path);

    }
    async log_out() {
        this.storage.clear();
        localStorage.clear();
        this.settings.customer.id = undefined;
        this.settings.vendor = false;
        this.settings.wishlist = [];
        await this.api.postItem('logout').subscribe(res => {}, err => {
            console.log(err);
        });
        if((<any>window).AccountKitPlugin)
        (<any>window).AccountKitPlugin.logout();
    }
    rateApp() {
        if (this.platform.is('cordova')) {
            this.appRate.preferences.storeAppURL = {
                ios: this.settings.settings.rate_app_ios_id,
                android: this.settings.settings.rate_app_android_id,
                windows: 'ms-windows-store://review/?ProductId=' + this.settings.settings.rate_app_windows_id
            };
            this.appRate.promptForRating(false);
        }
    }
    shareApp() {
        if (this.platform.is('cordova')) {
            var url = '';
            if (this.platform.is('android')) url = this.settings.settings.share_app_android_link;
            else url = this.settings.settings.share_app_ios_link;
            var options = {
                message: '',
                subject: '',
                files: ['', ''],
                url: url,
                chooserTitle: ''
            }
            this.socialSharing.shareWithOptions(options);
        }
    }
    email(contact) {
        let email = {
            to: contact,
            attachments: [],
            subject: '',
            body: '',
            isHtml: true
        };
        this.emailComposer.open(email);
    }
    ngOnInit() {
        // this.toggle = document.querySelector('#themeToggle');
        // this.toggle.addEventListener('ionChange', (ev) => {
        //   document.body.classList.toggle('dark', ev.detail.checked);
        
        //   if(ev.detail.checked) {
        //     this.statusBar.backgroundColorByHexString('#121212');
        //     this.statusBar.styleLightContent();
        //   } else {
        //     this.statusBar.backgroundColorByHexString('#ffffff');
        //     this.statusBar.styleDefault();
        //   }
        
        // });
        const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
        prefersDark.addListener((e) => checkToggle(e.matches));
        function loadApp() {
          checkToggle(prefersDark.matches);
        }
        function checkToggle(shouldCheck) {
          this.toggle.checked = shouldCheck;
        }
    }
    ionViewWillEnter() {
        this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent');
        this.storage.get('isDeliveryAgent').then((val) => {
            this.isDeliveryAgent = val;
        });

        if (this.isDeliveryAgent == 'true') {
            const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
            prefersDark.addListener((e) => checkToggle(e.matches));
            function loadApp() {
              checkToggle(prefersDark.matches);
            }
            function checkToggle(shouldCheck) {
              this.toggle.checked = shouldCheck;
            }
        }



        this.isVendorLogin = localStorage.getItem('isVendorLogin');

        this.storage.get('isVendorLogin').then((val) => {
            this.isVendorLogin = val;
        });
  
  
        if (this.isVendorLogin == 'true') {
          this.UserLoggedIn = false;
  
  
        }

        console.log(this.isDeliveryAgent);
        console.log(this.isDeliveryAgent);
        
    }
}