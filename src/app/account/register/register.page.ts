import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../../api.service';
import { Data } from '../../data';
import { Settings } from './../../data/settings';
import { FormBuilder, FormArray, Validators } from '@angular/forms';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare var google;

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    form: any;
    errors: any;
    status: any = {};
    disableSubmit: boolean = false;
    pushForm: any;
    phoneLogingInn: boolean = false;
    userInfo: any;
    phoneVerificationError: any;
    changeView : boolean = false;
    customerDeliveryAddString: any;
    DelLat: any;
    DelLng: any;
    address: any = [];
    countries: any;
    states: any;
    billingStates: any;
    shippingStates: any;
    disableButton: boolean = false;
    
    constructor(public platform: Platform, 
        public iab: InAppBrowser,
        private oneSignal: OneSignal, public api: ApiService, public data: Data, public loadingController: LoadingController, public settings: Settings, 
        public navCtrl: NavController, private fb: FormBuilder) {
        this.form = this.fb.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', Validators.email],
            phone: ['', Validators.required],
            password: ['', Validators.required],
          });

        //   this.formGro = this.fb.group({
        //     first_name: ['', Validators.required],
        //     last_name: ['', Validators.required],
        //     email: ['', Validators.email],
        //     phone: ['', Validators.required],
        //     password: ['', Validators.required],
        //   });
    }
    ngOnInit() {}
    async onSubmit() {
        this.disableSubmit = true;
        await this.api.postItem('create-user', this.form.value).subscribe(res => {
            this.status = res;
            if (this.status.errors) {
                this.errors = this.status.errors;
                this.disableSubmit = false;
                for (var key in this.errors) {
                    this.errors[key].forEach(item => item.replace('<strong>ERROR<\/strong>:', ''));
                }
            }
            else if (this.status.data != undefined) {
                this.settings.customer.id = this.status.ID;
                 if (this.platform.is('cordova'))
                    this.oneSignal.getIds().then((data: any) => {
                        this.pushForm.onesignal_user_id = data.userId;
                        this.pushForm.onesignal_push_token = data.pushToken;
                        this.api.postItem('update_user_notification', this.pushForm).subscribe(res =>{});
                    });
                this.changeView = true;
                this.disableSubmit = false;
            }
            else this.disableSubmit = false;
        }, err => {
            this.disableSubmit = false;
        });
    }
    loginWithPhone(){
        this.phoneLogingInn = true;
        (<any>window).AccountKitPlugin.loginWithPhoneNumber({
            useAccessToken: true,
            defaultCountryCode: "ID",
            facebookNotificationsEnabled: true,
          }, data => {
          (<any>window).AccountKitPlugin.getAccount(
            info => this.handlePhoneLogin(info),
            err => this.handlePhoneLogin(err));
          });
    }
    handlePhoneLogin(info){
        if(info.phoneNumber) {
            this.api.postItem('phone_number_login', {
                    "phone": info.phoneNumber,
                }).subscribe(res => {
                this.status = res;
                if (this.status.errors) {
                    this.errors = this.status.errors;
                } else if (this.status.data) {
                    this.settings.customer.id = this.status.ID;
                     if (this.platform.is('cordova')){
                        this.oneSignal.getIds().then((data: any) => {
                            this.form.onesignal_user_id = data.userId;
                            this.form.onesignal_push_token = data.pushToken;
                        });
                       this.api.postItem('update_user_notification', this.form).subscribe(res =>{});
                     }
                    if(this.status.allcaps.dc_vendor || this.status.allcaps.seller || this.status.allcaps.wcfm_vendor){
                        this.settings.vendor = true;
                    }
                    this.navCtrl.navigateBack('/tabs/account');
                }
                this.phoneLogingInn = false;
            }, err => {
                this.phoneLogingInn = false;
            });
        } else this.phoneLogingInn = false;
    }
    handlePhoneLoginError(error){
        this.phoneVerificationError = error;
        this.phoneLogingInn = false;
    }


    async saveAddress(){

        console.log('CoUN',this.countries);

        var loc  =  await this.getCurrentLocation();
      
        for (var key in this.settings.customer) {
            if (key === "billing" || key === "shipping") {
                for (var skey in this.settings.customer[key]) {
                    if (typeof this.settings.customer[key][skey] === "number") {
                        this.settings.customer[key][skey] = this.settings.customer[key][skey].toString();
                    }
                }
            }
        }
        
        console.log('??',loc);

        this.settings.customer.cus_lat = loc["lat"]
        this.settings.customer.cus_lng = loc["lng"]

        this.settings.customer.first_name = this.form.value.first_name;
        this.settings.customer.last_name = this.form.value.last_name;
        this.settings.customer.email = this.form.value.email;
        this.settings.customer.phone = this.form.value.phone;

        console.log('Before,', this.settings.customer);
        // return false;
        await this.api.WCV2put('customers/' + this.settings.customer.id, this.settings.customer).subscribe(res => {
            this.status = res;
            
            this.navCtrl.navigateBack('/tabs/account');
        }, err => {
        });

      
    }



    getCurrentLocation() {




        console.log(this.settings.customer.shipping.address_1);


        if (this.settings.customer.billing.address_1) {
            this.customerDeliveryAddString = '';
            this.customerDeliveryAddString = this.settings.customer.billing.address_1
                + ', ' + this.settings.customer.billing.address_2
                + ', ' + this.settings.customer.billing.city
                + ', ' + this.settings.customer.billing.country

        }

        if (this.settings.customer.shipping.address_1) {
            this.customerDeliveryAddString = '';
            this.customerDeliveryAddString = this.settings.customer.shipping.address_1
                + ', ' + this.settings.customer.shipping.address_2
                + ', ' + this.settings.customer.shipping.city
                + ', ' + this.settings.customer.shipping.country
        }

    

        return new Promise(resolve => {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': JSON.stringify(this.customerDeliveryAddString) }, function (results, status) {
                console.log(results);
                if (status == google.maps.GeocoderStatus.OK) {
                    this.DelLng = results[0].geometry.viewport.Ua.i;
                    this.DelLat = results[0].geometry.viewport.Ya.i;
                    console.log(this.DelLat);
                    console.log(this.DelLng);
                    resolve(
                        {
                            lat : this.DelLat,
                            lng : this.DelLng
                        });
                } else {

                    resolve(
                        {
                            lat : "",
                            lng : ""
                        });
                        
                    console.log("Unable to find address: " + status);
                }
            },
        );
        });


    }


    async getCountries() {
        await this.api.postItem('countries').subscribe(res => {
         

            this.countries = res;
            if (this.countries && this.countries.length == 1) {
                this.address['billing_country'] = this.countries[0].value;
                this.address['shipping_country'] = this.countries[0].value;
                this.billingStates = this.countries.find(item => item.value == this.address['billing_country']);
                this.shippingStates = this.countries.find(item => item.value == this.address['billing_country']);
            } else {
                this.billingStates = this.countries.find(item => item.value == this.settings.customer.billing.country);
                this.shippingStates = this.countries.find(item => item.value == this.settings.customer.shipping.country);
            }
        }, err => {
            console.log(err);
        });

        console.log(this.billingStates);


    }

    inAppRegister(v){

        if(v == 'ven'){
            var options = "https://fetch.betaplanets.com/vendor-register/";
        }

        if(v == 'drv'){
            var options = "https://fetch.betaplanets.com/driver-register/";
        }
   
        let browser = this.iab.create(options, '_blank', options);
        browser.show();
        // browser.on('loadstart').subscribe(data => {
        //     console.log(data);
            
        // });
        // browser.on('exit').subscribe(data => {
        //     console.log(data);
            
        // });
    }






}