import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { resetFakeAsyncZone } from '@angular/core/testing';

declare var google;

@Component({
    selector: 'app-edit-address',
    templateUrl: './edit-address.page.html',
    styleUrls: ['./edit-address.page.scss'],
})
export class EditAddressPage implements OnInit {
    address: any = [];
    countries: any;
    states: any;
    billingStates: any;
    shippingStates: any;
    status: any;
    disableButton: boolean = false;
    accountEdit = true;
    customerDeliveryAddString: any;
    DelLat: any;
    DelLng: any;
    constructor(public api: ApiService,
        public loadingController: LoadingController,
        public settings: Settings, public router: Router, public navCtrl: NavController, public route: ActivatedRoute) { }
    ngOnInit() {

        if (localStorage.getItem('accountEdit') === 'true') {
            this.accountEdit = false;
            // this.getCustomer();
        }

        this.getCustomer();

        this.getCountries();

        console.log('>>.', this.settings.customer.billing);

    }
    async getCountries() {
        await this.api.postItem('countries').subscribe(res => {
            console.log(res);

            this.countries = res;
            if (this.countries && this.countries.length == 1) {
                this.address['billing_country'] = this.countries[0].value;
                this.address['shipping_country'] = this.countries[0].value;
                this.billingStates = this.countries.find(item => item.value == this.address['billing_country']);
                this.shippingStates = this.countries.find(item => item.value == this.address['billing_country']);
            } else {
                this.billingStates = this.countries.find(item => item.value == this.settings.customer.billing.country);
                this.shippingStates = this.countries.find(item => item.value == this.settings.customer.shipping.country);
            }
        }, err => {
            console.log(err);
        });

        console.log(this.billingStates);


    }

    check() {
        console.log(this.billingStates.regions);
    }
    processAddress() {
        for (var key in this.settings.customer.billing) {
            this.address['billing_' + key] = this.settings.customer.billing[key];
        }
        for (var key in this.settings.customer.shipping) {
            this.address['shipping_' + key] = this.settings.customer.shipping[key];
        }
        // const postAddress = {
        //     billing_address: JSON.parse(JSON.stringify(this.settings.customer.billing)),
        //     shipping_address: JSON.parse(JSON.stringify(this.settings.customer.shipping)),
        // };
        this.updateAddress();
    }

    async updateAddress() {

        var loc  =  await this.getCurrentLocation();
        this.disableButton = true;
          for (var key in this.settings.customer) {
            if (key === "billing" || key === "shipping") {
                for (var skey in this.settings.customer[key]) {
                    if (typeof this.settings.customer[key][skey] === "number") {
                        this.settings.customer[key][skey] = this.settings.customer[key][skey].toString();
                    }
                }
            }
        }
         console.log('??',loc);

        this.settings.customer.cus_lat = loc["lat"]
        this.settings.customer.cus_lng = loc["lng"]
        console.log('Before,', this.settings.customer);
        await this.api.WCV2put('customers/' + this.settings.customer.id, this.settings.customer).subscribe(res => {
            this.status = res;
            this.disableButton = false;
        }, err => {
            this.disableButton = false;
        });
    }
    getBillingRegion() {
        this.billingStates = this.countries.find(item => item.value == this.settings.customer.billing.country);
        this.settings.customer.billing.state = '';

    }
    getShippingRegion() {
        this.shippingStates = this.countries.find(item => item.value == this.settings.customer.shipping.country);
        this.settings.customer.shipping.state = '';
    }

    async getCustomer() {
        const loading = await this.loadingController.create({
            message: 'Loading...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        await loading.present();
        // if (this.platform.is('hybrid'))
        // await this.api.WCV2getItemIonic('customers/' + this.settings.customer.id).then((res) => {
        //     this.settings.customer = res;
        //     loading.dismiss();
        // }, err => {
        //     console.log(err);
        //     loading.dismiss();
        // });
        // else {

        await this.api.WCV2getItem('customers/' + this.settings.customer.id).subscribe(res => {
            this.settings.customer = res;
            // this.getCurrentLocation();
            loading.dismiss();

        }, err => {
            console.log(err);
            loading.dismiss();
        });
        // }
    }

    ionViewWillEnter() {


        if (localStorage.getItem('accountEdit') === 'true') {
            this.accountEdit = false;
            this.getCustomer();
        }
        this.getCustomer();
        this.getCountries();
    }



    getCurrentLocation() {




        console.log(this.settings.customer.shipping.address_1);


        if (this.settings.customer.billing.address_1) {
            this.customerDeliveryAddString = '';
            this.customerDeliveryAddString = this.settings.customer.billing.address_1
                + ', ' + this.settings.customer.billing.address_2
                + ', ' + this.settings.customer.billing.city
                + ', ' + this.settings.customer.billing.country

        }

        if (this.settings.customer.shipping.address_1) {
            this.customerDeliveryAddString = '';
            this.customerDeliveryAddString = this.settings.customer.shipping.address_1
                + ', ' + this.settings.customer.shipping.address_2
                + ', ' + this.settings.customer.shipping.city
                + ', ' + this.settings.customer.shipping.country
        }

        let redirectAddress = this.settings.customer._billing_address_1
        if (this.settings.customer._billing_address_2) {
            redirectAddress += ", " + this.settings.customer._billing_address_2
        }
        if (this.settings.customer._billing_city) {
            redirectAddress += ", " + this.settings.customer._billing_city
        }
        if (this.settings.customer._billing_state) {
            redirectAddress += ", " + this.settings.customer._billing_state
        }


        return new Promise(resolve => {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': JSON.stringify(this.customerDeliveryAddString) }, function (results, status) {
                console.log(results);
                if (status == google.maps.GeocoderStatus.OK) {
                    this.DelLng = results[0].geometry.viewport.Ua.i;
                    this.DelLat = results[0].geometry.viewport.Ya.i;
                    console.log(this.DelLat);
                    console.log(this.DelLng);
                    resolve(
                        {
                            lat : this.DelLat,
                            lng : this.DelLng
                        });
                    // resolve(this.commonFiltere());
                } else {

                    resolve(
                        {
                            lat : "",
                            lng : ""
                        });
                        
                    console.log("Unable to find address: " + status);
                }
            },
        );
        });


    }





}