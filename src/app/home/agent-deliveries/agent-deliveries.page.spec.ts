import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgentDeliveriesPage } from './agent-deliveries.page';
import { ModalController } from '@ionic/angular';

describe('AgentDeliveriesPage', () => {
  let component: AgentDeliveriesPage;
  let fixture: ComponentFixture<AgentDeliveriesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentDeliveriesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgentDeliveriesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
