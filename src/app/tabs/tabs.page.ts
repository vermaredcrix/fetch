import { Component } from '@angular/core';
import { Data } from '../data';
import { Settings } from '../data/settings';
import { Storage } from '@ionic/storage';
import { Router, Event } from '@angular/router';
import { NavigationStart } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  isDeliveryAgent: any;
  isVendorLogin: any;
  UserLoggedIn: any;

  constructor(public data: Data,
    private storage: Storage,
    public settings: Settings,
    private router: Router,
  ) {

    if (this.isVendorLogin == 'true') {
      this.UserLoggedIn = false;
      console.log('this.isVendorLogin', this.isVendorLogin);

    }

    this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent')
    this.storage.get('isDeliveryAgent').then((val) => {

      // console.log('isDeliveryAgent', val);
      this.isDeliveryAgent = val;
    });
    // console.log('>>>>>>', this.isDeliveryAgent);
    this.router.events.subscribe((event: Event) => {

      this.isVendorLogin = localStorage.getItem('isVendorLogin');
      this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent')

      //if (event instanceof NavigationStart) {
      this.storage.get('isDeliveryAgent').then((val) => {
        // console.log('isDeliveryAgent', val);
        this.isDeliveryAgent = val;
      });

      this.storage.get('isVendorLogin').then((val) => {
        this.isVendorLogin = val;

        if (this.isVendorLogin == 'true') {
          this.UserLoggedIn = false;
          console.log('this.isVendorLogin', this.isVendorLogin);
        } else {
          this.UserLoggedIn = true;
        }
      });

      // console.log(' debug isVendorLogin'+this.isVendorLogin);
      // console.log(' debug isDeliveryAgent'+this.isDeliveryAgent);

      //}
    });
    this.isVendorLogin = localStorage.getItem('isVendorLogin');
    this.storage.get('isVendorLogin').then((val) => {
      this.isVendorLogin = val;
    });

  }

  //   ionViewWillEnter() {
  //     this.isDeliveryAgent = false;
  //     let deliveryBoyCheck = localStorage.getItem('isDeliveryAgent');

  //     this.storage.get('isDeliveryAgent').then((val) => {
  //         console.log('isDeliveryAgent', val);
  //         deliveryBoyCheck = val;
  //     });

  //     if(deliveryBoyCheck == 'true' ){
  //       console.log('TABSSSS');

  //         this.isDeliveryAgent = true;
  //     }


  // }

  ionViewWillEnter() {


    this.isVendorLogin = localStorage.getItem('isVendorLogin');
    this.isDeliveryAgent = localStorage.getItem('isDeliveryAgent')

    //if (event instanceof NavigationStart) {
    this.storage.get('isDeliveryAgent').then((val) => {
      // console.log('isDeliveryAgent', val);
      this.isDeliveryAgent = val;
    });

    this.storage.get('isVendorLogin').then((val) => {
      this.isVendorLogin = val;

      if (this.isVendorLogin == 'true') {
        this.UserLoggedIn = false;
        console.log('this.isVendorLogin', this.isVendorLogin);
      } else {
        this.UserLoggedIn = true;
      }
    });


    // console.log('TABS debug isVendorLogin'+this.isVendorLogin);
    // console.log('TABS debug isDeliveryAgent'+this.isDeliveryAgent);
  }


}
