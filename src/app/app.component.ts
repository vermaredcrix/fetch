import { ActivatedRoute } from "@angular/router";
import { Component } from "@angular/core";
import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AppMinimize } from "@ionic-native/app-minimize/ngx";
import { TranslateService } from "@ngx-translate/core";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { Config } from "./config";
import { ApiService } from '../app/api.service';
import { Settings } from '../app/data/settings';
import { Storage } from '@ionic/storage';

declare var wkWebView: any;

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
})
export class AppComponent {
  itemExpanded: boolean = false;
  public E_items: any = [];
  constructor(
    private config: Config,
    private nativeStorage: NativeStorage,
    public translateService: TranslateService,
    public platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private appMinimize: AppMinimize,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    private storage: Storage,
    public settings: Settings,
    public api: ApiService,
  ) {
 

  }

  ionViewDidLoad(){
    this.initializeApp();

    this.E_items = [{ expanded: false }];
  }
  public goToSettings(path) {
    this.navCtrl.navigateForward(path);
  }
  goTo(path) {
    // localStorage.clear();
    // if(path === 'tabs/account/address/edit-address'){
    //    console.log('setState');
    //    localStorage.setItem('accountEdit','true');

    // }
    this.navCtrl.navigateForward(path);
  }
  public expandItem(): void {
    this.itemExpanded = !this.itemExpanded;
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      document.addEventListener("deviceready", () => {
        wkWebView.injectCookie(this.config.url + "/");
      });

      this.statusBar.backgroundColorByHexString("#ffffff");

      /* Add your translation file in src/assets/i18n/ and set your default language here */
      this.translateService.setDefaultLang("en");
      //document.documentElement.setAttribute('dir', 'rtl');

      //document.documentElement.setAttribute('dir', 'rtl');

      //this.statusBar.backgroundColorByHexString('#004a91');
      //this.statusBar.backgroundColorByHexString('#ffffff');
      //this.statusBar.styleBlackTranslucent();
      //this.statusBar.styleLightContent();

      //this.minimize();
      this.platform.backButton.subscribeWithPriority(0, () => {
        this.appMinimize.minimize();
      });
    });
  }

  async LogOut(){
    this.storage.clear();
    localStorage.clear();
    this.settings.customer.id = undefined;
    this.settings.vendor = false;
    this.settings.wishlist = [];
    await this.api.postItem('logout').subscribe(res => {}, err => {
        console.log(err);
    });
    if((<any>window).AccountKitPlugin)
    (<any>window).AccountKitPlugin.logout();
  }
}